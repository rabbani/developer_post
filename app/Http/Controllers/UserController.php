<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        redirect(route('users.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
    }


    public function user_sort(Request $request)
    {
        $order = $request->order;
        $id = $request->id;

        $users = User::orderBy($id, $order)->get();
        $html = '';
        $sl = 1;
        if($order=='desc'){
            $sl= count($users);
            $new_order='asc';
        }else{
            $new_order='desc';
        }
        $html.=' <thead class="thead-light">
                    <tr>
                        <th class="column_sort" id="id" data-order="'.$new_order.'" scope="col">Sl <i class="fa fa-sort-amount-'.$order.'" aria-hidden="true"></i></th>
                        <th class="column_sort" id="name" data-order="'.$new_order.'" scope="col">Name <i class="fa fa-sort-amount-'.$order.'" aria-hidden="true"></i></th>
                        <th class="column_sort" id="email" data-order="'.$new_order.'" scope="col">Email <i class="fa fa-sort-amount-'.$order.'" aria-hidden="true"></i></th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>';
        $html.=' <tbody>';
        foreach ($users as $user):
            $html .= '<tr id="row'.$user->id.'">
                            <td>'.$sl.'</td>
                            <td>'.$user->name.'</td>
                            <td>'.$user->email.'</td>
                            <td>
                                <a href="javascript:void(0)"  data-email="'.$user->email.'" data-name="'.$user->name.'" class="edit-user" data-id="'.$user->id.'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="javascript:void(0)" class="delete-user" data-id="'.$user->id.'"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </td>
                        </tr>';
            if($order=='desc'){
                $sl--;
            }else {
                $sl++;
            }
        endforeach;
        $html.='</tbody>';
        return $html;
    }

    public function users_search(Request $request)
    {
        $value = $request->value;
        $users = User::where('name', 'LIKE', "%$value%")->orWhere('email', 'LIKE', "%$value%")->get();
        $html = '';
        $sl = 1;

        foreach ($users as $user):
            $html .= '<tr id="row'.$user->id.'">
                            <td>'.$sl.'</td>
                            <td>'.$user->name.'</td>
                            <td>'.$user->email.'</td>
                            <td>
                                <a href="javascript:void(0)"  data-email="'.$user->email.'" data-name="'.$user->name.'" class="edit-user" data-id="'.$user->id.'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="javascript:void(0)" class="delete-user" data-id="'.$user->id.'"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </td>
                        </tr>';

                $sl++;

        endforeach;

        return $html;
    }
}
