<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="{{ asset('js/jquery-3.5.1.min.js') }}"></script>
    <style>
        .column_sort {
            cursor: pointer;
        }
    </style>
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        @yield('content')
    </main>
</div>

<script src="{{ asset('js/bootstrap.min.js') }}" defer></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}" defer></script>

<script>

    // $(document).ready( function () {
    //     $('#user_table').DataTable();
    // } );

    $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

    });
    //save user
    $('#user_form_submit').on('click', function () {

        var name = $('#name').val();
        var email = $('#email').val();
        var password = $('#password').val();
        $.ajax({
            type: 'POST',
            url: "{{ route('users.store') }}",
            data: {name: name, password: password, email: email},
            success: function (data) {
                window.location.reload();

            }
        });
    });

    //save user
    $('#btn-user-update').on('click', function () {

        var user_id = $('#user_id').val();
        var user_name = $('#user_name').val();
        var user_email = $('#user_email').val();

        $.ajax({
            type: 'PUT',
            url: "{{ url('users') }}" + '/' + user_id,
            data: {name: user_name, email: user_email},
            success: function (data) {
                window.location.reload();

            }
        });
    });


    $('body').on('click', '.delete-user', function () {
        var user_id = $(this).data("id");
        confirm("Are You sure want to delete !");
        $.ajax({
            type: "DELETE",
            url: "{{ url('users')}}" + '/' + user_id,
            success: function (data) {
                $("#row" + user_id).remove();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });


    $('body').on('click', '.edit-user', function () {
        var user_id = $(this).data("id");
        var name = $(this).data("name");
        var email = $(this).data("email");
        $('#ajax-crud-modal').modal('show');
        $('#user_name').val(name);
        $('#user_email').val(email);
        $('#user_id').val(user_id);
    });

    $('body').on('click', '.column_sort', function () {

        var order = $(this).data('order');
        var id = $(this).attr('id');

        $.ajax({
            type: "POST",
            url: "{{ url('users-sort')}}",
            data: {order: order, id: id},
            success: function (data) {
                $('#user_table').html(data);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    })

    $('#search').on('keyup', function () {

        var value = $(this).val();

        $.ajax({
            type: "POST",
            url: "{{ url('users-search')}}",
            data: {value: value},
            success: function (data) {
                $('#table_body').html(data);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    })

</script>
</body>
</html>
