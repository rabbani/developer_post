@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-2">
                <div class="list-group">
                    <a href="/home" class="list-group-item list-group-item-action active">
                        Dashboard
                    </a>
                    <a href="#" class="list-group-item list-group-item-action">
                        Users
                    </a>
                </div>
            </div>
            <div class="col-md-10">
                <div class="card mb-5">
                    <div class="card-header">Create User</div>
                    <div class="card-body">
                        <form>
                            @csrf
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input name="name" type="text" class="form-control" id="name">
                            </div>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control" id="email"
                                       aria-describedby="emailHelp">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password">
                            </div>
                            <button id="user_form_submit" type="button" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="w-100"></div>
                <div style="display: flex; justify-content: center;">
                    <form class="form-inline">
                        <div class="form-group mx-sm-3 mb-2">
                            <label for="search"> Search user </label> &nbsp;
                            <input type="text" class="form-control" id="search" placeholder="Search here">
                        </div>
{{--                        <button type="submit" class="btn btn-primary mb-2">Search</button>--}}
                    </form>
                </div>
                <table id="user_table" class="table -table-striped mt-5 pt-5">
                    <thead class="thead-light">
                    <tr>
                        <th class="column_sort" id="id" data-order="desc" scope="col">Sl <i
                                class="fa fa-sort-amount-asc" aria-hidden="true"></i></th>
                        <th class="column_sort" id="name" data-order="desc" scope="col">Name <i
                                class="fa fa-sort-amount-asc" aria-hidden="true"></i></th>
                        <th class="column_sort" id="email" data-order="desc" scope="col">Email <i
                                class="fa fa-sort-amount-asc" aria-hidden="true"></i></th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody id="table_body">
                    @php($sl=1)
                    @foreach($users as $user)
                        <tr id="row{{$user->id}}">
                            <td>{{$sl}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>
                                <a href="javascript:void(0)" data-email="{{$user->email}}" data-name="{{$user->name}}"
                                   class="edit-user" data-id="{{$user->id}}"><i class="fa fa-pencil-square-o"
                                                                                aria-hidden="true"></i></a>
                                <a href="javascript:void(0)" class="delete-user" data-id="{{$user->id}}"><i
                                        class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        @php($sl++)
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="postCrudModal"></h4>
                </div>
                <div class="modal-body">
                    <form id="postForm" name="postForm" class="form-horizontal">
                        <input type="hidden" name="user_id" id="user_id">
                        <div class="form-group">
                            <label for="user_name" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="user_name" name="user_name" value=""
                                       required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-12">
                                <input class="form-control" id="user_email" name="user_email" value="" required="">
                            </div>
                        </div>
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="button" class="btn btn-primary" id="btn-user-update" value="update">Update
                            </button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>
@endsection

